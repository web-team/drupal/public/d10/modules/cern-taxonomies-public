# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.1] - 26/10/2023

- Updated `pathauto` to version `^1.12`.
- Updated `smart_trim` to version `^2.1`.

## [3.0.0] - 02/02/2023
- Web Team assumes responsibility for module.
- Ensure PHP 8.1 compatibility.
- Bump all first- and third-party dependencies.

## [2.0.2] - 13/10/2021

- Add D9 readiness

## [2.0.1] - 28/05/2021

- Add composer support
- Add CHANGELOG
